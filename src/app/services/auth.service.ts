import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable} from 'rxjs';
import { catchError, map, tap, flatMap } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { User } from '../shared/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  
  private usersUrl = 'http://159.89.50.39:5000/auth/login';  // URL to web api

  constructor(
    private http: HttpClient) { }

  /** GET cabins from the server */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl).pipe(catchError(this.handleError));
  }
  
  
  
  updateUsers(user: User): Observable<User> {
   return this.http.post<User>(this.usersUrl, user, httpOptions).pipe(catchError(this.handleError));
}

private handleError(error: Response): Observable<any> {
  console.error("observable error: ", error);
  return throwError(error.statusText);
}
  
  
  
  
}
