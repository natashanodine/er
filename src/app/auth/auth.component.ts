import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';


import { User } from '../shared/user';

import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

	user: User;
  users: User[];
  
   angForm: FormGroup;
   constructor(
   
   private fb: FormBuilder,
   private authService: AuthService
   ) {
    this.createForm();
  }
  
 
  ngOnInit() {
    this.getUsers();
  }
 
  getUsers(): void {
    this.authService.getUsers()
    .subscribe(users => this.users = users);
  }
  
  
   createForm() {
    this.angForm = this.fb.group({
       email: ['', Validators.required ],
       password: ['', Validators.required ]
    });

    this.angForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }
  
    onValueChanged(angFormData?: any) {
		
      this.user = this.angForm.value;
  }
   
   
   
   
   
  
  onSubmit() {
	  this.authService
  .updateUsers(this.user)
  .subscribe(user => this.users.push(user));
	  
	  
	   this.angForm.reset({
        email: '',
        password: ''
    });
  }
  

}
